export function setFirstName(state, data) {
    state.firstName = data
}
export function setLastName(state, data) {
    state.lastName = data
}
export function setEmail(state, data) {
    state.email = data
}
export function setPhone(state, data) {
    state.phone = data
}
export function setSelectedColors(state, data) {
    state.selectedColors = data
}
export function setPassword(state, data) {
    state.password = data
}
export function setReapeatPassword(state, data) {
    state.repeatPassword = data
}
