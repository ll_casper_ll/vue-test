
import { Notify } from 'quasar'
export function registerUser(/* context */) {
    Notify.create({
        message: 'You have successfully registered!',
        multiLine: true,
        color: 'positive',
        timeout: 10000,
        icon: 'done',
    })
    this.$router.push({ name: 'root' });
}
