export default function () {
  return {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    selectedColors: [],
    password: '',
    repeatPassword: '',
  }
}
